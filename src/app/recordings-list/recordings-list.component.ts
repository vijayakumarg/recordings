import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { URLS } from '../shared/constants/urls';

@Component({
  selector: 'app-recordings-list',
  templateUrl: './recordings-list.component.html',
  styleUrls: ['./recordings-list.component.scss']
})
export class RecordingsListComponent implements OnInit {

  constructor(private fb:FormBuilder,private http:HttpClient) { }

  recordingsListForm: FormGroup;
  public isProgress = false;
  public isDisabled = false;

  public clients = [
    { label: 'Bright House', value: 'brighthouse' },
    { label: 'TWC', value: 'twc' },
    { label: 'Charter', value: 'charter' }
  ];
  public projects = [
    { label: 'B2C', value: 'B2C' },
    { label: 'Mobile', value: 'mobile' }
  ]
  public tableData:any;

  ngOnInit(): void {
    this.recordingsListForm = this.fb.group({
      client:['',Validators.required],
      project:['',Validators.required]
    });
  }

  onSubmit(){
    this.isDisabled = true;
    this.isProgress = true;
    const params = new HttpParams()
    .set('client',this.recordingsListForm.value.client.value)
    .set('project',this.recordingsListForm.value.project.value)
    this.http.get(`${URLS.RECORDINGS.LIST}`,{params:params}).subscribe((result:any)=>{
      this.isDisabled = false;
      this.isProgress = false;
      this.tableData = result;
    },(error)=>{
      this.isProgress = false;
      this.isDisabled = false;
    });
  }

}
