import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from './shared/shared.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RecordingsCreateComponent } from './recordings-create/recordings-create.component';
import { RecordingsListComponent } from './recordings-list/recordings-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MessageService } from 'primeng/api';


@NgModule({
  declarations: [
    AppComponent,
    RecordingsCreateComponent,
    RecordingsListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [HttpClientModule,MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
