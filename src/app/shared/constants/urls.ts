export const BASE_URL = `https://i9hveccohg.execute-api.us-east-1.amazonaws.com/Prod/files`;

export const URLS = {
    RECORDINGS: {
        CREATE: `${BASE_URL}/upload`,
        LIST: `${BASE_URL}/recordings`
    }
}
