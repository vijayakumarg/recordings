import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordingsCreateComponent } from './recordings-create.component';

describe('RecordingsCreateComponent', () => {
  let component: RecordingsCreateComponent;
  let fixture: ComponentFixture<RecordingsCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecordingsCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordingsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
