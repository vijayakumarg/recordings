import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import {URLS} from './../shared/constants/urls';

@Component({
  selector: 'app-recordings-create',
  templateUrl: './recordings-create.component.html',
  styleUrls: ['./recordings-create.component.scss'],
  providers: [MessageService]
})
export class RecordingsCreateComponent implements OnInit {

  constructor(private fb:FormBuilder,private http:HttpClient,private messageService:MessageService,
    private router:Router) { }
  recordingsForm:FormGroup;
  selectedFile:any;
  isProgress: boolean = false;
  isCompleted: boolean = false;
  isDisabled = false;

  public clients = [
    { label: 'Bright House', value: 'brighthouse' },
    { label: 'TWC', value: 'twc' },
    { label: 'Charter', value: 'charter' }
  ];
  public projects = [
    { label: 'B2C', value: 'B2C' },
    { label: 'Mobile', value: 'mobile' }
  ]

  ngOnInit(): void {
    this.recordingsForm = this.fb.group({
      client: ['',Validators.required],
      project:['',Validators.required],
      upload: ['',Validators.required]
    })
  }

  onFileSelect(event:any){
    this.selectedFile = event.target.files[0];
    this.recordingsForm.get('upload')?.setValue(this.selectedFile);
  }

  onSubmit(){
    this.isProgress = true;
    this.isDisabled = true;
    let chosenFile = this.recordingsForm.get('upload')?.value;
    const params = new HttpParams()
    .set("client",this.recordingsForm.value.client.value)
    .set("project",this.recordingsForm.value.project.value);
    const formData = new FormData();
    formData.append("upload",chosenFile);
    this.http.post(`${URLS.RECORDINGS.CREATE}`,formData,{params:params}).subscribe((data:any)=>{
      this.isProgress = false;
      this.isCompleted = true;
      if(data.key){
        this.messageService.add({
          severity: 'success', summary: 'Success', detail: 'Recordings Created Successfully!'
        });
      }else{
        this.messageService.add({
          severity: 'error',
          summary: `Insufficient Privileges`,
          detail: `failed`
        });
      }
      this.router.navigate(['/recordings-list'])
      this.recordingsForm.reset();
    }, (error) => {
      this.isProgress = false;
      this.isCompleted = false;
      this.isDisabled = false;
    })
  }

}
