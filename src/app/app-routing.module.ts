import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecordingsCreateComponent } from './recordings-create/recordings-create.component';
import { RecordingsListComponent } from './recordings-list/recordings-list.component';

const routes: Routes = [{
  path:'',
  pathMatch: 'full',
  redirectTo:'recordings-create'
},
{
  path:'recordings-create',
  component: RecordingsCreateComponent
},
{
  path:'recordings-list',
  component: RecordingsListComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
